import serial
import tkinter as tk
from math import log10

# open serial
# change port if needed
# to find active COM port copy this to python interactive terminal (without hashtags):
# import serial.tools.list_ports as port_list
# ports = list(port_list.comports())
# for p in ports:
#     print (p)
ser = serial.Serial(port='COM4')

# initialize values
values = [0, 0, 0]
izero_values = [0, 0, 0]
value_cells = [[], []]
value_labels_text = ['Red', 'Green', 'Blue']
izero_isset = False
# create window
window = tk.Tk()
# set font
window.option_add('*Font', 'Times 28')
label_warning = tk.Label(master=window, text='I zero is not set! Showing raw values', foreground='red')
label_warning.pack(fill=tk.X, padx=2, pady=2)

frame_grid = tk.Frame(master=window)
frame_grid.pack(side=tk.LEFT, padx=2, pady=2)
# first row: headings
tk.Label(master=frame_grid, text='I', width=10).grid(row=0, column=1)
tk.Label(master=frame_grid, text='I zero', width=10).grid(row=0, column=2)
# first column: red, green and blue labels
for i in range(3):
    tk.Label(master=frame_grid, text=value_labels_text[i]).grid(row=i+1, column=0)
    value_cells[0].append(tk.Label(master=frame_grid, text=''))
    value_cells[0][i].grid(row=i+1, column=1)
    value_cells[1].append(tk.Label(master=frame_grid, text=''))
    value_cells[1][i].grid(row=i+1, column=2)

# Read values from COM port
def get_values():
    global values
    try:
        inputstring = ser.readline().decode().strip()
        values = [int(n) for n in inputstring.split(',')]
    except:
        print('[*] Couldn\'t read the data')
    ser.flushInput()

# print the values
def update():
    get_values()
    for i in range(3):
        try:
            current_value = '%.4f' % (-1 * log10(values[i] / izero_values[i])) if izero_isset else values[i]
        except:
            print('[*] Math exception')
            current_value = 'N/A'
        # print(values[i], izero_values[i], current_value)
        value_cells[0][i].config(text=current_value)
        value_cells[1][i].config(text=izero_values[i])
    window.after(500, update)

# function for button
def update_izero():
    global izero_isset
    if 0 not in values:
        izero_isset = True
        label_warning.pack_forget()
        for i in range(3):
            izero_values[i] = values[i]

button_set = tk.Button(master=window, text='Update I zero', command=update_izero, relief=tk.RAISED, borderwidth=1)
button_set.pack(fill=tk.Y, side=tk.RIGHT)

update()
window.mainloop()
