# Colormeter lux to absorption converter

## What is it
Python script with Tkinter GUI to read data from colormeter via COM port, store values and use them to calculate light absorption with Beer–Lambert–Bouguer law

## Installation
### Prerequisites
- python3: [Download for Windows](https://www.python.org/downloads/windows/)  
    Check `Add python.exe to PATH` box  
    Use `Install Now` or choose `Customize Installation` and check `pip` and `td/tk and IDLE`
- pyserial: `python3 -m pip install pyserial`

### Downloading source code
- using git: `git clone https://gitlab.com/0xFF0xE4/colormeter-lux-to-absorption-converter.git`
- via browser: Use `Download` button (between Web IDE and Clone) and choose `zip`

## Running the script
- open terminal in the directory where the script is located
- run python3 .\lux-to-absorb.py

## Common problems
- if you see the following error: `serial.serialutil.SerialException: could not open port 'COM4': FileNotFoundError(2, 'The system cannot find the file specified.', None, 2)`  
it means that either your colormeter is not connected or it is connected to another COM port. Open the script and follow instructions in lines 7-11 to detect and change COM port
